@Formspage
Feature: Verification of Forms Page on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the forms tile
  later verify all the items in the forms page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Forms" Tile
    Then I am on Forms page

  Scenario: Verification of Practice Form
    And I click "Practice Form"
    Then I see "Practice Form" appears
