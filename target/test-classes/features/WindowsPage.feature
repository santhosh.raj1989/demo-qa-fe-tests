@Windowspage
Feature: Verification of Windows Page on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the Windows tile
  later verify all the items in the forms page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Alerts, Frame & Windows" Tile
    Then I am on Alerts, Frame & Windows page

  Scenario: Verification of Browser Window
    And I click the "Browser Windows"
    And I see "Browser Windows" opens

  Scenario: Verification of Alerts
    And I click the "Alerts"
    And I see "Alerts" opens

  Scenario: Verification of Frames
    And I click the "Frames"
    And I see "Frames" opens

  Scenario: Verification of Nested Frames
    And I click the "Nested Frames"
    And I see "Nested Frames" opens

  Scenario: Verification of Modal Dialogs
    And I click the "Modal Dialogs"
    And I see "Modal Dialogs" opens