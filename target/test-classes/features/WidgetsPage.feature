@Widgetspage
Feature: Verification of Widgets Tile on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the Widgets tile
  later verify all the items in the Widgets page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Widgets" Tile
    Then I am on Widgets page

  Scenario: Verification of Accordian
    And I click "Accordian" Widgets
    And I see "Accordian" widget

  Scenario: Verification of Auto Complete
    And I click "Auto Complete" Widgets
    And I see "Auto Complete" widget

  Scenario: Verification of Date Picker
    And I click "Date Picker" Widgets
    And I see "Date Picker" widget

  Scenario: Verification of Slider
    And I click "Slider" Widgets
    And I see "Slider" widget

  Scenario: Verification of Progress Bar
    And I click "Progress Bar" Widgets
    And I see "Progress Bar" widget

  Scenario: Verification of Tabs
    And I click "Tabs" Widgets
    And I see "Tabs" widget

  Scenario: Verification of Tool Tips
    And I click "Tool Tips" Widgets
    And I see "Tool Tips" widget

  Scenario: Verification of Menu
    And I click "Menu" Widgets
    And I see "Menu" widget

  Scenario: Verification of Select Menu
    And I click "Select Menu" Widgets
    And I see "Select Menu" widget
