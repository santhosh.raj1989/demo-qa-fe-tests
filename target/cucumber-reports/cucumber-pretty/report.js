$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ElementsPage.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Elements Tile on Tools QA page",
  "description": "\nAs a user,\n\nI should be able to visit \"Tools QA\" webpage successfully and click on the elements tile\nlater verify all the items in the elements page",
  "id": "verification-of-elements-tile-on-tools-qa-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Elementspage"
    }
  ]
});
formatter.before({
  "duration": 6729135488,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 145900804,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 105568000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2077661759,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 28185119,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Text Box",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-text-box",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I click \"Text Box\" Element",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I see \"Text Box\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Text Box",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 70199995,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Text Box",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 35501907,
  "status": "passed"
});
formatter.after({
  "duration": 132440068,
  "status": "passed"
});
formatter.before({
  "duration": 6202778264,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 17864773,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 118785075,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058616337,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 29006576,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Verification of Elements Tile",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-elements-tile",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "I click \"Check Box\" Element",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "I see \"Check Box\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Check Box",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 61577372,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Check Box",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 19160464,
  "status": "passed"
});
formatter.after({
  "duration": 82755203,
  "status": "passed"
});
formatter.before({
  "duration": 5832505798,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15918347,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 121217819,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2052042154,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 43151981,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Verification of Radio Button",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-radio-button",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 30,
  "name": "I click \"Radio Button\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I see \"Radio Button\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Radio Button",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 94696333,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Radio Button",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 26146791,
  "status": "passed"
});
formatter.after({
  "duration": 90379415,
  "status": "passed"
});
formatter.before({
  "duration": 6355634993,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15873235,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 107476874,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2053114190,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 21354553,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Verification of Web Tables",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-web-tables",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "I click \"Web Tables\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I see \"Web Tables\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Web Tables",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 84872324,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Web Tables",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 40160175,
  "status": "passed"
});
formatter.after({
  "duration": 81690245,
  "status": "passed"
});
formatter.before({
  "duration": 5693770726,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15378906,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 148772758,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2072214250,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 21399065,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Verification of Buttons",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-buttons",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "I click \"Buttons\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "I see \"Buttons\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Buttons",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 82764780,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Buttons",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 46567041,
  "status": "passed"
});
formatter.after({
  "duration": 82485229,
  "status": "passed"
});
formatter.before({
  "duration": 6200160362,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 25553183,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 164038719,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2069313428,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 20602500,
  "status": "passed"
});
formatter.scenario({
  "line": 41,
  "name": "Verification of Links",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-links",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 42,
  "name": "I click \"Links\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "I see \"Links\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Links",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 62613377,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Links",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 40498325,
  "status": "passed"
});
formatter.after({
  "duration": 132168864,
  "status": "passed"
});
formatter.before({
  "duration": 5399791326,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 25177057,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 133829774,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2109205434,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 21443370,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Verification of BrokenLinks",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-brokenlinks",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 46,
  "name": "I click \"BrokenLinks\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "I see \"BrokenLinks\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "BrokenLinks",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 56590527,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "BrokenLinks",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 2721298524,
  "status": "passed"
});
formatter.after({
  "duration": 85166105,
  "status": "passed"
});
formatter.before({
  "duration": 5037712582,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 20655819,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 120696817,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058565796,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 21081373,
  "status": "passed"
});
formatter.scenario({
  "line": 49,
  "name": "Verification of UploadDownload",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-uploaddownload",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 50,
  "name": "I click \"UploadDownload\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "I see \"UploadDownload\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "UploadDownload",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 73568882,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "UploadDownload",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 49378893,
  "status": "passed"
});
formatter.after({
  "duration": 80132720,
  "status": "passed"
});
formatter.before({
  "duration": 5200226676,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Elements\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Elements page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16756469,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 114779985,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Elements",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2066157514,
  "status": "passed"
});
formatter.match({
  "location": "ElementsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 20438387,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "Verification of DynamicProperties",
  "description": "",
  "id": "verification-of-elements-tile-on-tools-qa-page;verification-of-dynamicproperties",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 54,
  "name": "I click \"DynamicProperties\" Element",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "I see \"DynamicProperties\" is displayed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "DynamicProperties",
      "offset": 9
    }
  ],
  "location": "ElementsSteps.iClickElements(String)"
});
formatter.result({
  "duration": 56200155,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DynamicProperties",
      "offset": 7
    }
  ],
  "location": "ElementsSteps.iSeeIsDisplayed(String)"
});
formatter.result({
  "duration": 47015977,
  "status": "passed"
});
formatter.after({
  "duration": 75541510,
  "status": "passed"
});
formatter.uri("FormsPage.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Forms Page on Tools QA page",
  "description": "\nAs a user,\n\nI should be able to visit \"Tools QA\" webpage successfully and click on the forms tile\nlater verify all the items in the forms page",
  "id": "verification-of-forms-page-on-tools-qa-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Formspage"
    }
  ]
});
formatter.before({
  "duration": 5186473738,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Forms\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Forms page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 14250891,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 96574007,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Forms",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2061900534,
  "status": "passed"
});
formatter.match({
  "location": "FormsSteps.iAmOnFormsPage()"
});
formatter.result({
  "duration": 22776410,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Practice Form",
  "description": "",
  "id": "verification-of-forms-page-on-tools-qa-page;verification-of-practice-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I click \"Practice Form\"",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I see \"Practice Form\" appears",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Practice Form",
      "offset": 9
    }
  ],
  "location": "FormsSteps.iClickForms(String)"
});
formatter.result({
  "duration": 89476790,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Practice Form",
      "offset": 7
    }
  ],
  "location": "FormsSteps.iSeeAppears(String)"
});
formatter.result({
  "duration": 22075780,
  "status": "passed"
});
formatter.after({
  "duration": 83018908,
  "status": "passed"
});
formatter.uri("HomePage.feature");
formatter.feature({
  "line": 2,
  "name": "Tools QA Web page content verification",
  "description": "\nAs a user,\n\nI should be able to visit Tools QA webpage successfully and verify the Content of Tools QA page\nwhich includes correctness of Titles, Images and Footer.",
  "id": "tools-qa-web-page-content-verification",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Homepage"
    }
  ]
});
formatter.before({
  "duration": 5061755360,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15332474,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Verification of Tiles",
  "description": "",
  "id": "tools-qa-web-page-content-verification;verification-of-tiles",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 14
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 15
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 16
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 17
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 18
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 19
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 116572693,
  "status": "passed"
});
formatter.after({
  "duration": 80365777,
  "status": "passed"
});
formatter.before({
  "duration": 5187971048,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16375611,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Selenium Certification Training Banner Image",
  "description": "",
  "id": "tools-qa-web-page-content-verification;verification-of-selenium-certification-training-banner-image",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I see Selenium Certification Banner Image",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.iSeeSeleniumCertificationBannerImage()"
});
formatter.result({
  "duration": 17954177,
  "status": "passed"
});
formatter.after({
  "duration": 88705953,
  "status": "passed"
});
formatter.before({
  "duration": 4770077471,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15663220,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Verification of footer",
  "description": "",
  "id": "tools-qa-web-page-content-verification;verification-of-footer",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "I see footer information as follows",
  "rows": [
    {
      "cells": [
        "© 2013-2020 TOOLSQA.COM",
        "ALL RIGHTS RESERVED."
      ],
      "line": 26
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.iSeeFooterInformationAsFollows(DataTable)"
});
formatter.result({
  "duration": 18748962,
  "status": "passed"
});
formatter.after({
  "duration": 78205453,
  "status": "passed"
});
formatter.uri("InteractionsPage.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Interactions Page on Tools QA page",
  "description": "\nAs a user,\n\nI should be able to visit \"Tools QA\" webpage successfully and click on the Interactions tile\nlater verify all the items in the forms page",
  "id": "verification-of-interactions-page-on-tools-qa-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Interactionpage"
    }
  ]
});
formatter.before({
  "duration": 6064196089,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Interactions\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Interactions page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16481334,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 111100387,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Interactions",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2062546613,
  "status": "passed"
});
formatter.match({
  "location": "InteractionsSteps.iAmOnInteractionsPage()"
});
formatter.result({
  "duration": 22254385,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Sortable Interactions",
  "description": "",
  "id": "verification-of-interactions-page-on-tools-qa-page;verification-of-sortable-interactions",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I click the \"Sortable\" Interaction",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Sortable",
      "offset": 13
    }
  ],
  "location": "InteractionsSteps.iClickTheInteraction(String)"
});
formatter.result({
  "duration": 73766543,
  "status": "passed"
});
formatter.after({
  "duration": 92299818,
  "status": "passed"
});
formatter.before({
  "duration": 5777718059,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Interactions\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Interactions page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 24768080,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 147596872,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Interactions",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058961795,
  "status": "passed"
});
formatter.match({
  "location": "InteractionsSteps.iAmOnInteractionsPage()"
});
formatter.result({
  "duration": 20032500,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Verification of Selectable Interactions",
  "description": "",
  "id": "verification-of-interactions-page-on-tools-qa-page;verification-of-selectable-interactions",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "I click the \"Selectable\" Interaction",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Selectable",
      "offset": 13
    }
  ],
  "location": "InteractionsSteps.iClickTheInteraction(String)"
});
formatter.result({
  "duration": 69128035,
  "status": "passed"
});
formatter.after({
  "duration": 79371221,
  "status": "passed"
});
formatter.before({
  "duration": 5004722753,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Interactions\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Interactions page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 17156335,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 107484287,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Interactions",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2061006703,
  "status": "passed"
});
formatter.match({
  "location": "InteractionsSteps.iAmOnInteractionsPage()"
});
formatter.result({
  "duration": 19919834,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Verification of Resizable Interactions",
  "description": "",
  "id": "verification-of-interactions-page-on-tools-qa-page;verification-of-resizable-interactions",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 28,
  "name": "I click the \"Resizable\" Interaction",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Resizable",
      "offset": 13
    }
  ],
  "location": "InteractionsSteps.iClickTheInteraction(String)"
});
formatter.result({
  "duration": 56514036,
  "status": "passed"
});
formatter.after({
  "duration": 78654749,
  "status": "passed"
});
formatter.before({
  "duration": 5259402456,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Interactions\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Interactions page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16736287,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 125412974,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Interactions",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2061446187,
  "status": "passed"
});
formatter.match({
  "location": "InteractionsSteps.iAmOnInteractionsPage()"
});
formatter.result({
  "duration": 20542072,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Verification of Draggable Interactions",
  "description": "",
  "id": "verification-of-interactions-page-on-tools-qa-page;verification-of-draggable-interactions",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 31,
  "name": "I click the \"Draggable\" Interaction",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Draggable",
      "offset": 13
    }
  ],
  "location": "InteractionsSteps.iClickTheInteraction(String)"
});
formatter.result({
  "duration": 1104933150,
  "error_message": "org.openqa.selenium.ElementClickInterceptedException: element click intercepted: Element \u003cli class\u003d\"btn btn-light \" id\u003d\"item-4\"\u003e...\u003c/li\u003e is not clickable at point (133, 661). Other element would receive the click: \u003cdiv\u003e...\u003c/div\u003e\n  (Session info: headless chrome\u003d93.0.4577.82)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027Santhosh-MacBook-Pro.local\u0027, ip: \u0027fe80:0:0:0:1c0d:e564:e23:745%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.16\u0027, java.version: \u002714.0.1\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 93.0.4577.82, chrome: {chromedriverVersion: 93.0.4577.63 (ff5c0da2ec0ad..., userDataDir: /var/folders/d7/clj20lrn35z...}, goog:chromeOptions: {debuggerAddress: localhost:52747}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\nSession ID: 9edc69dd8eeb64df22d6e66c85c478c3\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.base/java.lang.reflect.Constructor.newInstanceWithCaller(Constructor.java:500)\n\tat java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:481)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:285)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:84)\n\tat jdk.internal.reflect.GeneratedMethodAccessor13.invoke(Unknown Source)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:564)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:51)\n\tat com.sun.proxy.$Proxy21.click(Unknown Source)\n\tat PageObjects.InteractionsPage.verifySubItems(InteractionsPage.java:69)\n\tat StepDefinitions.InteractionsSteps.iClickTheInteraction(InteractionsSteps.java:32)\n\tat ✽.And I click the \"Draggable\" Interaction(InteractionsPage.feature:31)\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 410811274,
  "status": "passed"
});
formatter.before({
  "duration": 5086578756,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Interactions\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Interactions page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16460966,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 119510109,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Interactions",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2070647611,
  "status": "passed"
});
formatter.match({
  "location": "InteractionsSteps.iAmOnInteractionsPage()"
});
formatter.result({
  "duration": 19608125,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Verification of Droppable Interactions",
  "description": "",
  "id": "verification-of-interactions-page-on-tools-qa-page;verification-of-droppable-interactions",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "I click the \"Droppable\" Interaction",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Droppable",
      "offset": 13
    }
  ],
  "location": "InteractionsSteps.iClickTheInteraction(String)"
});
formatter.result({
  "duration": 79672390,
  "status": "passed"
});
formatter.after({
  "duration": 82920739,
  "status": "passed"
});
formatter.uri("WidgetsPage.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Widgets Tile on Tools QA page",
  "description": "\nAs a user,\n\nI should be able to visit \"Tools QA\" webpage successfully and click on the Widgets tile\nlater verify all the items in the Widgets page",
  "id": "verification-of-widgets-tile-on-tools-qa-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Widgetspage"
    }
  ]
});
formatter.before({
  "duration": 5425394603,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 19992023,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 132035470,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2056855980,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 26223360,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Accordian",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-accordian",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I click \"Accordian\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I see \"Accordian\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Accordian",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 72026899,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Accordian",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 51921735,
  "status": "passed"
});
formatter.after({
  "duration": 82654548,
  "status": "passed"
});
formatter.before({
  "duration": 5357540332,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 21053099,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 112233693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058689613,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 26993148,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Verification of Auto Complete",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-auto-complete",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "I click \"Auto Complete\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I see \"Auto Complete\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Complete",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 90800607,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Auto Complete",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 42284154,
  "status": "passed"
});
formatter.after({
  "duration": 82147744,
  "status": "passed"
});
formatter.before({
  "duration": 5487116025,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 17473099,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 107665818,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2059024898,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 22461499,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Verification of Date Picker",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-date-picker",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 30,
  "name": "I click \"Date Picker\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I see \"Date Picker\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Date Picker",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 69233257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Date Picker",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 162428547,
  "status": "passed"
});
formatter.after({
  "duration": 78143561,
  "status": "passed"
});
formatter.before({
  "duration": 5181878111,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15328645,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 110184187,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058331737,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 19835190,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Verification of Slider",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-slider",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "I click \"Slider\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I see \"Slider\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Slider",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 60576943,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slider",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 409874646,
  "status": "passed"
});
formatter.after({
  "duration": 91562551,
  "status": "passed"
});
formatter.before({
  "duration": 5457699149,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 21772530,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 107650421,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058277747,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 33300740,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Verification of Progress Bar",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-progress-bar",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "I click \"Progress Bar\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "I see \"Progress Bar\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Progress Bar",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 84005315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Progress Bar",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 10101002936,
  "status": "passed"
});
formatter.after({
  "duration": 80046880,
  "status": "passed"
});
formatter.before({
  "duration": 5136675864,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 21007536,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 121353063,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2059485749,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 20605927,
  "status": "passed"
});
formatter.scenario({
  "line": 41,
  "name": "Verification of Tabs",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-tabs",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 42,
  "name": "I click \"Tabs\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "I see \"Tabs\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Tabs",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 57884001,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Tabs",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 41281237,
  "status": "passed"
});
formatter.after({
  "duration": 78006032,
  "status": "passed"
});
formatter.before({
  "duration": 5325386298,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 26767891,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 187528090,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2066392466,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 20428996,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Verification of Tool Tips",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-tool-tips",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 46,
  "name": "I click \"Tool Tips\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "I see \"Tool Tips\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Tool Tips",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 67273328,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Tool Tips",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 138478797,
  "status": "passed"
});
formatter.after({
  "duration": 87117646,
  "status": "passed"
});
formatter.before({
  "duration": 5390480443,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 16760829,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 109603791,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2059449272,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 20255815,
  "status": "passed"
});
formatter.scenario({
  "line": 49,
  "name": "Verification of Menu",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-menu",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 50,
  "name": "I click \"Menu\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "I see \"Menu\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Menu",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 67605881,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Menu",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 53778081,
  "status": "passed"
});
formatter.after({
  "duration": 79185064,
  "status": "passed"
});
formatter.before({
  "duration": 5227371740,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Widgets\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Widgets page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 17664003,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 117907063,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Widgets",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2056794447,
  "status": "passed"
});
formatter.match({
  "location": "WidgetsSteps.iAmOnWidgetsPage()"
});
formatter.result({
  "duration": 20615402,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "Verification of Select Menu",
  "description": "",
  "id": "verification-of-widgets-tile-on-tools-qa-page;verification-of-select-menu",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 54,
  "name": "I click \"Select Menu\" Widgets",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "I see \"Select Menu\" widget",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Select Menu",
      "offset": 9
    }
  ],
  "location": "WidgetsSteps.iClickWidgets(String)"
});
formatter.result({
  "duration": 92605352,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Select Menu",
      "offset": 7
    }
  ],
  "location": "WidgetsSteps.iSeeWidget(String)"
});
formatter.result({
  "duration": 63054793,
  "status": "passed"
});
formatter.after({
  "duration": 87172710,
  "status": "passed"
});
formatter.uri("WindowsPage.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Windows Page on Tools QA page",
  "description": "\nAs a user,\n\nI should be able to visit \"Tools QA\" webpage successfully and click on the Windows tile\nlater verify all the items in the forms page",
  "id": "verification-of-windows-page-on-tools-qa-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Windowspage"
    }
  ]
});
formatter.before({
  "duration": 5390185772,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Alerts, Frame \u0026 Windows\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Alerts, Frame \u0026 Windows page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 22478914,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 157944428,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts, Frame \u0026 Windows",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2085585359,
  "status": "passed"
});
formatter.match({
  "location": "WindowsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 22063452,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verification of Browser Window",
  "description": "",
  "id": "verification-of-windows-page-on-tools-qa-page;verification-of-browser-window",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 22,
  "name": "I click the \"Browser Windows\"",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I see \"Browser Windows\" opens",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Browser Windows",
      "offset": 13
    }
  ],
  "location": "WindowsSteps.iClickThe(String)"
});
formatter.result({
  "duration": 72188071,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Browser Windows",
      "offset": 7
    }
  ],
  "location": "WindowsSteps.iSeeOpens(String)"
});
formatter.result({
  "duration": 95236203,
  "status": "passed"
});
formatter.after({
  "duration": 81376720,
  "status": "passed"
});
formatter.before({
  "duration": 5301020671,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Alerts, Frame \u0026 Windows\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Alerts, Frame \u0026 Windows page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15834708,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 106892283,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts, Frame \u0026 Windows",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2059268404,
  "status": "passed"
});
formatter.match({
  "location": "WindowsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 20226173,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Verification of Alerts",
  "description": "",
  "id": "verification-of-windows-page-on-tools-qa-page;verification-of-alerts",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "I click the \"Alerts\"",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I see \"Alerts\" opens",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts",
      "offset": 13
    }
  ],
  "location": "WindowsSteps.iClickThe(String)"
});
formatter.result({
  "duration": 57174642,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts",
      "offset": 7
    }
  ],
  "location": "WindowsSteps.iSeeOpens(String)"
});
formatter.result({
  "duration": 54396724,
  "status": "passed"
});
formatter.after({
  "duration": 76723239,
  "status": "passed"
});
formatter.before({
  "duration": 5243335872,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Alerts, Frame \u0026 Windows\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Alerts, Frame \u0026 Windows page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15441311,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 112048856,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts, Frame \u0026 Windows",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2057206320,
  "status": "passed"
});
formatter.match({
  "location": "WindowsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 19867247,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Verification of Frames",
  "description": "",
  "id": "verification-of-windows-page-on-tools-qa-page;verification-of-frames",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 30,
  "name": "I click the \"Frames\"",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I see \"Frames\" opens",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Frames",
      "offset": 13
    }
  ],
  "location": "WindowsSteps.iClickThe(String)"
});
formatter.result({
  "duration": 59982830,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Frames",
      "offset": 7
    }
  ],
  "location": "WindowsSteps.iSeeOpens(String)"
});
formatter.result({
  "duration": 165761735,
  "status": "passed"
});
formatter.after({
  "duration": 77436596,
  "status": "passed"
});
formatter.before({
  "duration": 5258481197,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Alerts, Frame \u0026 Windows\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Alerts, Frame \u0026 Windows page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 15906658,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 114450327,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts, Frame \u0026 Windows",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2061064554,
  "status": "passed"
});
formatter.match({
  "location": "WindowsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 21742741,
  "status": "passed"
});
formatter.scenario({
  "line": 33,
  "name": "Verification of Nested Frames",
  "description": "",
  "id": "verification-of-windows-page-on-tools-qa-page;verification-of-nested-frames",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "I click the \"Nested Frames\"",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I see \"Nested Frames\" opens",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Nested Frames",
      "offset": 13
    }
  ],
  "location": "WindowsSteps.iClickThe(String)"
});
formatter.result({
  "duration": 71732718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nested Frames",
      "offset": 7
    }
  ],
  "location": "WindowsSteps.iSeeOpens(String)"
});
formatter.result({
  "duration": 213249393,
  "status": "passed"
});
formatter.after({
  "duration": 84341024,
  "status": "passed"
});
formatter.before({
  "duration": 5256135279,
  "status": "passed"
});
formatter.background({
  "line": 9,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 10,
  "name": "I am on Tools QA Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I see following tiles on the home page",
  "rows": [
    {
      "cells": [
        "Elements"
      ],
      "line": 12
    },
    {
      "cells": [
        "Forms"
      ],
      "line": 13
    },
    {
      "cells": [
        "Alerts, Frame \u0026 Windows"
      ],
      "line": 14
    },
    {
      "cells": [
        "Widgets"
      ],
      "line": 15
    },
    {
      "cells": [
        "Interactions"
      ],
      "line": 16
    },
    {
      "cells": [
        "Book Store Application"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click \"Alerts, Frame \u0026 Windows\" Tile",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I am on Alerts, Frame \u0026 Windows page",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeSteps.HomePage()"
});
formatter.result({
  "duration": 17858532,
  "status": "passed"
});
formatter.match({
  "location": "HomeSteps.iSeeFollowingTilesOnTheHomePage(DataTable)"
});
formatter.result({
  "duration": 105141338,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Alerts, Frame \u0026 Windows",
      "offset": 9
    }
  ],
  "location": "HomeSteps.iClickTile(String)"
});
formatter.result({
  "duration": 2058278215,
  "status": "passed"
});
formatter.match({
  "location": "WindowsSteps.iAmOnElementsPage()"
});
formatter.result({
  "duration": 20205270,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Verification of Modal Dialogs",
  "description": "",
  "id": "verification-of-windows-page-on-tools-qa-page;verification-of-modal-dialogs",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "I click the \"Modal Dialogs\"",
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "I see \"Modal Dialogs\" opens",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Modal Dialogs",
      "offset": 13
    }
  ],
  "location": "WindowsSteps.iClickThe(String)"
});
formatter.result({
  "duration": 57444228,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Modal Dialogs",
      "offset": 7
    }
  ],
  "location": "WindowsSteps.iSeeOpens(String)"
});
formatter.result({
  "duration": 137253274,
  "status": "passed"
});
formatter.after({
  "duration": 78539384,
  "status": "passed"
});
});