package Utilities;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BaseClass {

    private static WebDriver driver;
    private static WebDriverWait wait;
    JavascriptExecutor js = (JavascriptExecutor) driver;

    public BaseClass(WebDriver driver, WebDriverWait wait) {
        BaseClass.driver = driver;
        BaseClass.wait = wait;
    }

    public void WaitUntilElementVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void scrollTillBottom(){
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
    }

    public void pause(int time)
    {
        try {
            Thread.sleep(time);
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }
}