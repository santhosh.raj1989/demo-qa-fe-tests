package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ElementPage extends BaseClass {

    private WebDriver driver = Hooks.driver;

    public ElementPage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "main-header")
    
    private WebElement subpagesTitle;

    @FindBy(xpath = "//li[contains(.,'Text Box')]")
    
    private WebElement textBox;

    @FindBy(xpath = "//li[contains(.,'Check Box')]")
    
    private WebElement checkBox;

    @FindBy(xpath = "//li[contains(.,'Radio Button')]")
    
    private WebElement radioButton;

    @FindBy(xpath = "//li[contains(.,'Web Tables')]")
    
    private WebElement webTables;

    @FindBy(xpath = "//li[contains(.,'Buttons')]")
    
    private WebElement buttons;

    @FindBy(xpath = "//li[contains(.,'Links')]")
    
    private WebElement links;

    @FindBy(xpath = "//li[contains(.,'Broken Links - Images')]")
    
    private WebElement brokenlinks;

    @FindBy(xpath = "//li[contains(.,'Upload and Download')]")
    
    private WebElement uploadNDownload;

    @FindBy(xpath = "//li[contains(.,'Dynamic Properties')]")
    
    private WebElement dynamicProperties;

    @FindBy(id = "userName")
    
    private WebElement userName;

    @FindBy(className = "rct-node-icon")
    
    private WebElement chkBox;

    @FindBy(xpath = "//label[@for='yesRadio']")
    
    private WebElement radButton;

    @FindBy(xpath = "//div[@class='rt-resizable-header-content']")
    
    private WebElement wbTable;

    @FindBy(id = "doubleClickBtn")
    
    private WebElement btns;

    @FindBy(id = "simpleLink")
    
    private WebElement smplLink;

    @FindBy(linkText = "Click Here for Broken Link")
    
    private WebElement brknLink;

    @FindBy(linkText = "Download")
    
    private WebElement dwnld;

    @FindBy(xpath = "//span[text()='Dynamic Properties']")
    
    private WebElement dynProp;

    public String checkElementsTile() {
        return subpagesTitle.getText();
    }

    public void verifySubItems(String item) {
        switch (item) {
            case "Text Box":
                textBox.click();
                break;
            case "Check Box":
                checkBox.click();
                break;
            case "Radio Button":
                radioButton.click();
                break;
            case "Web Tables":
                webTables.click();
                break;
            case "Buttons":
                scrollTillBottom();
                buttons.click();
                break;
            case "Links":
                scrollTillBottom();
                links.click();
                break;
            case "BrokenLinks":
                scrollTillBottom();
                brokenlinks.click();
                break;
            case "UploadDownload":
                scrollTillBottom();
                uploadNDownload.click();
                break;
            case "DynamicProperties":
                scrollTillBottom();
                dynamicProperties.click();
                break;
            default:
                System.out.println("Invalid");
        }


    }

    public void verifyDisplaySubItems(String item) {
        switch (item) {
            case "Text Box":
                userName.isDisplayed();
                break;
            case "Check Box":
                chkBox.isSelected();
                break;
            case "Radio Button":
                radButton.isSelected();
                break;
            case "Web Tables":
                wbTable.isDisplayed();
                break;
            case "Buttons":
                btns.click();
                break;
            case "Links":
                smplLink.click();
                break;
            case "BrokenLinks":
                brknLink.click();
                break;
            case "UploadDownload":
                scrollTillBottom();
                dwnld.click();
                break;
            case "DynamicProperties":
                scrollTillBottom();
                dynProp.click();
                break;
            default:
                System.out.println("Invalid");
        }

    }
}