package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InteractionsPage extends BaseClass {

    private WebDriver driver = Hooks.driver;

    public InteractionsPage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "main-header")

    private WebElement subpagesTitle;

    @FindBy(xpath = "//li[contains(.,'Sortable')]")
    
    private WebElement sortable;

    @FindBy(xpath = "//li[contains(.,'Selectable')]")
    
    private WebElement selectable;

    @FindBy(xpath = "//li[contains(.,'Resizable')]")
    
    private WebElement resizable;

    @FindBy(xpath = "//li[contains(.,'Droppable')]")
    
    private WebElement droppable;

    @FindBy(xpath = "//li[contains(.,'Dragabble')]")
    
    private WebElement draggable;

    public String checkElementsTile() {
        return subpagesTitle.getText();
    }

    public void verifySubItems(String subitem) {
        switch (subitem) {
            case "Sortable":
                scrollTillBottom();
                sortable.click();
                break;
            case "Selectable":
                scrollTillBottom();
                selectable.click();
                break;
            case "Resizable":
                scrollTillBottom();
                resizable.click();
                break;
            case "Droppable":
                scrollTillBottom();
                droppable.click();
                break;
            case "Draggable":
                scrollTillBottom();
                draggable.click();
                break;
            default:
                System.out.println("Invalid");
        }

    }
}