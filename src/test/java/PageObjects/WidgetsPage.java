package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WidgetsPage extends BaseClass {

    private WebDriver driver = Hooks.driver;

    public WidgetsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }


    @FindBy(className = "main-header")
    
    private WebElement subpagesTitle;

    @FindBy(xpath = "//li[contains(.,'Accordian')]")
    
    private WebElement accordian;

    @FindBy(id = "section1Heading")
    
    private WebElement accordianClk;

    @FindBy(xpath = "//li[contains(.,'Auto Complete')]")
    
    private WebElement autoComplete;

    @FindBy(xpath = "//div[contains(@class,'auto-complete__value-container auto-complete__value-container--is-multi')]")
    
    private WebElement autoCompleteAction;

    @FindBy(xpath = "//li[contains(.,'Date Picker')]")
    
    private WebElement datePicker;

    @FindBy(id = "datePickerMonthYearInput")
    
    private WebElement datePickerAction;

    @FindBy(xpath = "//li[contains(.,'Slider')]")
    
    private WebElement slider;

    @FindBy(xpath = "//li[contains(.,'Progress Bar')]")
    
    private WebElement progressBar;

    @FindBy(id = "startStopButton")
    
    private WebElement progressBarAction;

    @FindBy(xpath = "//li[contains(.,'Tabs')]")
    
    private WebElement tabs;

    @FindBy(id = "demo-tab-what")
    
    private WebElement tabsAction;

    @FindBy(xpath = "//li[contains(.,'Tool Tips')]")
    
    private WebElement toolTip;

    @FindBy(id = "toolTipTextField")
    
    private WebElement toolTipAction;

    @FindBy(xpath = "//li[contains(.,'Menu')]")
    
    private WebElement menu;

    @FindBy(linkText = "Main Item 1")
    
    private WebElement menuAction;

    @FindBy(xpath = "//li[contains(.,'Select Menu')]")
    
    private WebElement selectMenu;

    @FindBy(xpath = "//div[text()='Select Option']")
    
    private WebElement selectMenuAction;

    public String checkElementsTile() {
        return subpagesTitle.getText();
    }

    public void verifySubItems(String item) {
        switch (item) {
            case "Accordian":
                scrollTillBottom();
                accordian.click();
                break;
            case "Auto Complete":
                scrollTillBottom();
                autoComplete.click();
                break;
            case "Date Picker":
                scrollTillBottom();
                datePicker.click();
                break;
            case "Slider":
                scrollTillBottom();
                slider.click();
                break;
            case "Progress Bar":
                scrollTillBottom();
                progressBar.click();
                break;
            case "Tabs":
                scrollTillBottom();
                tabs.click();
                break;
            case "Tool Tips":
                scrollTillBottom();
                toolTip.click();
                break;
            case "Menu":
                scrollTillBottom();
                menu.click();
                break;
            case "Select Menu":
                scrollTillBottom();
                selectMenu.click();
                break;
            default:
                System.out.println("Invalid");
        }
    }

    public void verifySubItemsWidgetDisplay(String item) {
        switch (item) {
            case "Accordian":
                accordianClk.click();
                break;
            case "Auto Complete":
                autoComplete.isDisplayed();
                break;
            case "Date Picker":
                datePickerAction.click();
                break;
            case "Slider":
                WebElement slider = driver.findElement(By.xpath("//input[@class='range-slider " +
                        "range-slider--primary']"));
                Actions move = new Actions(driver);
                Action action = (Action) move.dragAndDropBy(slider, 30, 0).build();
                action.perform();
                break;
            case "Progress Bar":
                progressBarAction.click();
                long timeoutInSeconds = 10;
                new WebDriverWait(driver, timeoutInSeconds)
                        .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='100%']")));
                break;
            case "Tabs":
                tabsAction.click();
                break;
            case "Tool Tips":
                Actions hover = new Actions(driver);
                WebElement we = driver.findElement(By.id("toolTipTextField"));
                hover.moveToElement(we).build().perform();
                break;
            case "Menu":
                menuAction.click();
                break;
            case "Select Menu":
                selectMenuAction.click();
                break;
            default:
                System.out.println("Invalid");
        }
    }
}