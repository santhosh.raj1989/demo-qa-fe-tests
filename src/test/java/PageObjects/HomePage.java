package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;

public class HomePage extends BaseClass {
    private WebDriver driver = Hooks.driver;

    public HomePage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='card mt-4 top-card']")
    private List<WebElement> tile;

    @FindBy(css = "div#app>footer>span")
    private WebElement footer;

    public boolean homePageIsDisplayed() {
        WebElement logoImage = driver.findElement(By.xpath("//img[@src='/images/Toolsqa.jpg']"));
        Boolean imageStatus = (Boolean) ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].complete "
                        + "&& typeof arguments[0].naturalWidth != \"undefined\" "
                        + "&& arguments[0].naturalWidth > 0", logoImage);
        return imageStatus;
    }

    public boolean bannerIsDisplayed() {
        WebElement liveBannerImage = driver.findElement(By.xpath("//div[@id='app']//img"));
        Boolean imageStatus = (Boolean) ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].complete "
                        + "&& typeof arguments[0].naturalWidth != \"undefined\" "
                        + "&& arguments[0].naturalWidth > 0", liveBannerImage);
        return imageStatus;
    }

    public String elementsTileDisplayed() {
        return tile.get(0).getText();
    }

    public String formsTileDisplayed() {
        return tile.get(1).getText();
    }

    public String alertsTileDisplayed() {
        return tile.get(2).getText();
    }

    public String widgetsTileDisplayed() {
        return tile.get(3).getText();
    }

    public String interactionsTileDisplayed() {
        return tile.get(4).getText();
    }

    public String booksStoresTileDisplayed() {
        return tile.get(5).getText();
    }

    public String verifyFooter() {
        return footer.getText();
    }

    public boolean clickElementsTile(String tileName) {
        switch (tileName) {
            case "Elements":
                tile.get(0).click();
                pause(2000);
                break;
            case "Forms":
                tile.get(1).click();
                pause(2000);
                break;
            case "Alerts, Frame & Windows":
                tile.get(2).click();
                pause(2000);
                break;
            case "Widgets":
                tile.get(3).click();
                pause(2000);
                break;
            case "Interactions":
                tile.get(4).click();
                pause(2000);
                break;
        }
        return true;
    }

}