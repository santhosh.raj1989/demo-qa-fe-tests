package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormsPage extends BaseClass {

    private WebDriver driver = Hooks.driver;

    public FormsPage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "main-header")
    
    private WebElement subpagesTitle;

    @FindBy(xpath = "//li[contains(.,'Practice Form')]")
    
    private WebElement practiceForm;

    @FindBy(id = "firstName")
    
    private WebElement practiceFormDisp;


    public String checkTileName() {
        return subpagesTitle.getText();
    }

    public void verifySubItems(String item) {
        if ("Practice Form".equals(item)) {
            practiceForm.click();
        } else {
            System.out.println("Invalid");
        }
    }

    public void verifySubItemsDisplay(String item) {
        if ("Practice Form".equals(item)) {
            practiceFormDisp.isDisplayed();
        } else {
            System.out.println("Invalid");
        }
    }
}