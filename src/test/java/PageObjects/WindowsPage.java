package PageObjects;

import StepDefinitions.Hooks;
import Utilities.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WindowsPage extends BaseClass {
    //    public static WebDriver driver;
    private WebDriver driver = Hooks.driver;

    public WindowsPage(WebDriver driver, WebDriverWait wait) {

        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

//    private final String headerMainMenu = "//div[@class='row main-menu-merchant']";
//    private final String cartMenu = "//div[@class='navbar-cart']//a[@href='/cart']";

    @FindBy(className = "main-header")
    
    private WebElement subpagesTitle;

    @FindBy(xpath = "//li[contains(.,'Browser Windows')]")
    
    private WebElement browserwindows;

    @FindBy(xpath = "//li[contains(.,'Alerts')]")
    
    private WebElement alerts;

    @FindBy(xpath = "//li[contains(.,'Frames')]")
    
    private WebElement frames;

    @FindBy(xpath = "//li[contains(.,'Nested Frames')]")
    
    private WebElement nestedFrames;

    @FindBy(xpath = "//li[contains(.,'Modal Dialogs')]")
    
    private WebElement modalDialogs;

    @FindBy(xpath = "//li[contains(.,'Links')]")
    
    private WebElement links;

    @FindBy(xpath = "//li[contains(.,'Broken Links - Images')]")
    
    private WebElement brokenlinks;

    @FindBy(xpath = "//li[contains(.,'Upload and Download')]")
    
    private WebElement uploadNDownload;

    @FindBy(xpath = "//li[contains(.,'Dynamic Properties')]")
    
    private WebElement dynamicProperties;

    @FindBy(id = "tabButton")
    
    private WebElement newTab;

    @FindBy(id = "alertButton")
    
    private WebElement newalertTab;

    @FindBy(id = "showSmallModal")
    
    private WebElement smallMdlTab;

    @FindBy(id = "closeSmallModal")
    
    private WebElement smallMdlTabClose;

//
//    @FindBy(xpath = "//img[@alt='Selenium Online Training']")
//    
//    private WebElement liveBanner;
//
//    @FindBy(xpath = "//img[@alt='Advertisement']")
//    
//    private WebElement googleAds;


    public String checkElementsTile() {
//        WaitUntilElementVisible(subpagesTitle);
        return subpagesTitle.getText();
    }

    public void verifySubItems(String subitem) {
        switch (subitem) {
            case "Browser Windows":
                browserwindows.click();
                break;
            case "Alerts":
                scrollTillBottom();
                alerts.click();
                break;
            case "Frames":
                scrollTillBottom();
                frames.click();
                break;
            case "Nested Frames":
                scrollTillBottom();
                nestedFrames.click();
                break;
            case "Modal Dialogs":
                scrollTillBottom();
                modalDialogs.click();
                break;
            default:
                System.out.println("Invalid");
        }
    }

    public void verifySubItemsLaunches(String subitem) {
        switch (subitem) {
            case "Browser Windows":
                newTab.click();
                for (String winHandle : driver.getWindowHandles())
                    driver.switchTo().window(winHandle);
                break;
            case "Alerts":
                newalertTab.click();
                driver.switchTo().alert().accept();
                break;
            case "Frames":
                scrollTillBottom();
                frames.click();
                driver.switchTo().frame(1);
                break;
            case "Nested Frames":
                scrollTillBottom();
                nestedFrames.click();
                driver.switchTo().frame(1);
                break;
            case "Modal Dialogs":
                scrollTillBottom();
                smallMdlTab.click();
                driver.switchTo().activeElement();
                smallMdlTabClose.click();
                break;
            default:
                System.out.println("Invalid");
        }
    }
}