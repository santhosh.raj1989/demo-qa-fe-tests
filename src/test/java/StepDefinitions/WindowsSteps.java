package StepDefinitions;

import PageObjects.WindowsPage;
import Utilities.PropertiesReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WindowsSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    WindowsPage windowsPage = new WindowsPage(driver, wait);

    public WindowsSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    // *** Then ***

    @Then("^I am on Alerts, Frame & Windows page$")
    public void iAmOnElementsPage() {
        Assert.assertEquals("Alerts, Frame & Windows",
                windowsPage.checkElementsTile());
    }

    @Then("^I see \"([^\"]*)\" opens$")
    public void iSeeOpens(String item) {
        windowsPage.verifySubItemsLaunches(item);
    }

    // *** When ***

    @When("^I click \"([^\"]*)\" Window$")
    public void iClickElements(String item) {

    }

    @When("^I click the \"([^\"]*)\"$")
    public void iClickThe(String subitem) {
        windowsPage.verifySubItems(subitem);
    }

}