package StepDefinitions;

import PageObjects.ElementPage;
import Utilities.PropertiesReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ElementsSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    ElementPage elementPage = new ElementPage(driver, wait);

    public ElementsSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    // *** Then ***

    @When("^I click \"([^\"]*)\" Element$")
    public void iClickElements(String item) {
        elementPage.verifySubItems(item);
    }

    // *** Then ***

    @Then("^I am on Elements page$")
    public void iAmOnElementsPage() {
        Assert.assertEquals("Elements", elementPage.checkElementsTile());
    }

    @Then("^I see \"([^\"]*)\" is displayed$")
    public void iSeeIsDisplayed(String item) {
        elementPage.verifyDisplaySubItems(item);
    }

}