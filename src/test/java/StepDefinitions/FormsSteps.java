package StepDefinitions;

import PageObjects.ElementPage;
import PageObjects.FormsPage;
import PageObjects.HomePage;
import Utilities.PropertiesReader;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FormsSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    FormsPage formsPage = new FormsPage(driver, wait);

    public FormsSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    // *** When ***

    @When("^I click \"([^\"]*)\"$")
    public void iClickForms(String item) {
        formsPage.verifySubItems(item);
    }

    // *** Then ***

    @Then("^I am on Forms page$")
    public void iAmOnFormsPage() {
        Assert.assertEquals("Forms", formsPage.checkTileName());
    }

    @Then("^I see \"([^\"]*)\" appears$")
    public void iSeeAppears(String item) {
        formsPage.verifySubItemsDisplay(item);
    }
}