package StepDefinitions;

import PageObjects.HomePage;
import Utilities.PropertiesReader;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomeSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    HomePage homePage = new HomePage(driver, wait);

    public HomeSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    // *** Given ***

    @Given("I am on Tools QA Home Page")
    public void HomePage() {
        Assert.assertTrue(homePage.homePageIsDisplayed());
    }

    // *** When ***

    @When("^I click \"([^\"]*)\" Tile$")
    public void iClickTile(String tileName) {
        homePage.clickElementsTile(tileName);
    }

    // *** Then ***

    @Then("^I see following tiles on the home page$")
    public void iSeeFollowingTilesOnTheHomePage(DataTable testData) {
        List<String> details = testData.asList(String.class);
        if (details.get(0).equals("Elements")) {
            Assert.assertEquals(details.get(0), homePage.elementsTileDisplayed());
        }
        if (details.get(1).equals("Forms")) {
            Assert.assertEquals(details.get(1), homePage.formsTileDisplayed());
        }
        if (details.get(2).equals("Alerts, Frame & Windows")) {
            Assert.assertEquals(details.get(2), homePage.alertsTileDisplayed());
        }
        if (details.get(3).equals("Widgets")) {
            Assert.assertEquals(details.get(3), homePage.widgetsTileDisplayed());
        }
        if (details.get(4).equals("Interactions")) {
            Assert.assertEquals(details.get(4), homePage.interactionsTileDisplayed());
        }
        if (details.get(5).equals("Book Store Application")) {
            Assert.assertEquals(details.get(5), homePage.booksStoresTileDisplayed());
        }
    }

    @Then("^I see Selenium Certification Banner Image$")
    public void iSeeSeleniumCertificationBannerImage() {
        Assert.assertTrue(homePage.bannerIsDisplayed());
    }

    @Then("^I see footer information as follows$")
    public void iSeeFooterInformationAsFollows(DataTable testData) {
        List<String> footerDetails = testData.asList(String.class);
        Assert.assertTrue(homePage.verifyFooter().contains(footerDetails.get(0)));
    }

}