package StepDefinitions;

import PageObjects.InteractionsPage;
import Utilities.PropertiesReader;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class InteractionsSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    InteractionsPage interactionsPage = new InteractionsPage(driver, wait);

    public InteractionsSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    @Then("^I am on Interactions page$")
    public void iAmOnInteractionsPage() {
        Assert.assertEquals("Interactions", interactionsPage.checkElementsTile());
    }

    @When("^I click the \"([^\"]*)\" Interaction$")
    public void iClickTheInteraction(String subitem)  {
            interactionsPage.verifySubItems(subitem);
}
}