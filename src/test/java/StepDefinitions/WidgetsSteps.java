package StepDefinitions;

import PageObjects.WidgetsPage;
import Utilities.PropertiesReader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WidgetsSteps {

    private WebDriver driver = Hooks.driver;
    private WebDriverWait wait;
    WidgetsPage WidgetPage = new WidgetsPage(driver, wait);

    public WidgetsSteps() throws Exception {
        PropertiesReader propertiesReader = new PropertiesReader();
        this.wait = new WebDriverWait(driver, propertiesReader.getTimeout());
    }

    // *** Then ***

    @Then("^I am on Widgets page$")
    public void iAmOnWidgetsPage() {
        Assert.assertEquals("Widgets", WidgetPage.checkElementsTile());
    }

    @Then("^I see \"([^\"]*)\" widget$")
    public void iSeeWidget(String item) {
        WidgetPage.verifySubItemsWidgetDisplay(item);

    }

    // *** When ***

    @When("^I click \"([^\"]*)\" Widgets$")
    public void iClickWidgets(String item) {
        WidgetPage.verifySubItems(item);
    }
}