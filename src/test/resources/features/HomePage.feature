@Homepage
Feature: Tools QA Web page content verification

  As a user,

  I should be able to visit Tools QA webpage successfully and verify the Content of Tools QA page
  which includes correctness of Titles, Images and Footer.

  Background:
    Given I am on Tools QA Home Page

  Scenario: Verification of Tiles
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |

  Scenario: Verification of Selenium Certification Training Banner Image
    Then I see Selenium Certification Banner Image

  Scenario: Verification of footer
    Then I see footer information as follows
      | © 2013-2020 TOOLSQA.COM | ALL RIGHTS RESERVED. |