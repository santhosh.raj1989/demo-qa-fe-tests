@Widgetspage
Feature: Verification of Widgets Tile on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the Widgets tile
  later verify all the items in the Widgets page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Widgets" Tile
    Then I am on Widgets page

  Scenario: Verification of Accordian
    When I click "Accordian" Widgets
    Then I see "Accordian" widget

  Scenario: Verification of Auto Complete
    When I click "Auto Complete" Widgets
    Then I see "Auto Complete" widget

  Scenario: Verification of Date Picker
    When I click "Date Picker" Widgets
    Then I see "Date Picker" widget

  Scenario: Verification of Slider
    When I click "Slider" Widgets
    Then I see "Slider" widget

  Scenario: Verification of Progress Bar
    When I click "Progress Bar" Widgets
    Then I see "Progress Bar" widget

  Scenario: Verification of Tabs
    When I click "Tabs" Widgets
    Then I see "Tabs" widget

  Scenario: Verification of Tool Tips
    When I click "Tool Tips" Widgets
    Then I see "Tool Tips" widget

  Scenario: Verification of Menu
    When I click "Menu" Widgets
    Then I see "Menu" widget

  Scenario: Verification of Select Menu
    When I click "Select Menu" Widgets
    Then I see "Select Menu" widget
