@Interactionpage
Feature: Verification of Interactions Page on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the Interactions tile
  later verify all the items in the forms page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Interactions" Tile
    Then I am on Interactions page

  Scenario: Verification of Sortable Interactions
    And I click the "Sortable" Interaction

  Scenario: Verification of Selectable Interactions
    And I click the "Selectable" Interaction

  Scenario: Verification of Resizable Interactions
    And I click the "Resizable" Interaction

  Scenario: Verification of Draggable Interactions
    And I click the "Draggable" Interaction

  Scenario: Verification of Droppable Interactions
    And I click the "Droppable" Interaction