@Windowspage
Feature: Verification of Windows Page on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the Windows tile
  later verify all the items in the forms page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Alerts, Frame & Windows" Tile
    Then I am on Alerts, Frame & Windows page

  Scenario: Verification of Browser Window
    When I click the "Browser Windows"
    Then I see "Browser Windows" opens

  Scenario: Verification of Alerts
    When I click the "Alerts"
    Then I see "Alerts" opens

  Scenario: Verification of Frames
    When I click the "Frames"
    Then I see "Frames" opens

  Scenario: Verification of Nested Frames
    When I click the "Nested Frames"
    Then I see "Nested Frames" opens

  Scenario: Verification of Modal Dialogs
    When I click the "Modal Dialogs"
    Then I see "Modal Dialogs" opens