@Elementspage
Feature: Verification of Elements Tile on Tools QA page

  As a user,

  I should be able to visit "Tools QA" webpage successfully and click on the elements tile
  later verify all the items in the elements page

  Background:
    Given I am on Tools QA Home Page
    Then I see following tiles on the home page
      | Elements                |
      | Forms                   |
      | Alerts, Frame & Windows |
      | Widgets                 |
      | Interactions            |
      | Book Store Application  |
    When I click "Elements" Tile
    Then I am on Elements page

  Scenario: Verification of Text Box
    When I click "Text Box" Element
    Then I see "Text Box" is displayed

  Scenario: Verification of Elements Tile
    When I click "Check Box" Element
    Then I see "Check Box" is displayed

  Scenario: Verification of Radio Button
    When I click "Radio Button" Element
    Then I see "Radio Button" is displayed

  Scenario: Verification of Web Tables
    When I click "Web Tables" Element
    Then I see "Web Tables" is displayed

  Scenario: Verification of Buttons
    When I click "Buttons" Element
    Then I see "Buttons" is displayed

  Scenario: Verification of Links
    When I click "Links" Element
    Then I see "Links" is displayed

  Scenario: Verification of BrokenLinks
    When I click "BrokenLinks" Element
    Then I see "BrokenLinks" is displayed

  Scenario: Verification of UploadDownload
    When I click "UploadDownload" Element
    Then I see "UploadDownload" is displayed

  Scenario: Verification of DynamicProperties
    And I click "DynamicProperties" Element
    Then I see "DynamicProperties" is displayed
